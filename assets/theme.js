/* This source file is never referenced. This script is minified and inlined. */

/* Theme priority check: localStorage > system > "light" */
var theme = localStorage.getItem("theme-pref"); // "light", "dark", or null
if (!theme) // no localStorage (e.g. first run)
  // set to "dark" based on system, or set to "light" as a fallback
  theme = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
// invariant/assertion: by now theme must be either "light" or "dark"

/* Returns true only if theme is currently dark */
function isDark() {
  return theme === "dark";
}

/* Apply the current theme to the page (sync localStorage & DOM) */
function applyTheme() {
  // Set localStorage preference
  localStorage.setItem("theme-pref", theme);
  // Set content for the color-scheme meta tag
  document.getElementById("cs").content = theme;
  // Set media attribute for the dark stylesheet
  document.getElementById("ds").media = isDark() ? "screen" : "not all";
}

/* This runs before page load to minimize flickering */
applyTheme();



/* The rest runs after the theme switcher has loaded */

var btn = document.getElementById("theme-toggle");

function syncARIA() {
  btn.setAttribute("aria-pressed", isDark());
}

syncARIA();
btn.setAttribute("aria-label", "Tema scuro"));
btn.style.display = "inline-block"; // as a flex fallback

function switchTheme() {
  theme = isDark() ? "light" : "dark";
  applyTheme();
  syncARIA();
}

btn.onclick = switchTheme;
