# Hello World

I work as a freelance [Simplicity Web Coach][s]. Feel free to get in touch. ✉️

_P.S._ This site is powered by [Hugo][h] + [GitLab][g] + [Netlify][n] + [Porkbun][p]. 🚀️

_P.P.S._ I also blog at [42m.me][b] 👽️

[s]: https://www.michelenuzzolese.com/en/
[h]: https://gohugo.io/
[g]: https://gitlab.com/
[n]: https://www.netlify.com/
[p]: https://porkbun.com/
[b]: https://42m.me
