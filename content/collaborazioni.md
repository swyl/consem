---
title: "Facciamo rete!"
seoTitle: "Collaborazioni"
description: "Sono alla continua ricerca di gente competente che offre servizi per freelance. Se condividiamo etica professionale e valori, facciamo rete! 🕸️"
---

Sono aperto a collaborazioni con gente competente che condivida almeno in parte etica professionale e alcuni [valori](/chi-sono/#valori) fondamentali.
{.big-text}

## Chi cerco

Mi piacerebbe fare rete con chi si occupa di:

* Grafica web, con attenzione all'UX e all'accessibilità.
* SEO e marketing, ma non quello da imbonitori e fuffaguru.
* Servizi collaterali a supporto di freelance che comunicano online.

Quest'ultima categoria non è limitata alle nuove professioni digitali, per cui potrebbe essere sorprendentemente vasta. C'è qualche esempio più avanti.

## Che faccio io

Aiuto freelance (e micro-imprese) che vogliono creare o gestire il proprio sito a:

* Presentarsi in modo professionale e senza fronzoli.
* Tagliare sprechi di tempo, soldi ed energie.
* Non perdere la testa con il fai da te.

In pratica, _semplifico_. Per questo sono il <em>consulente web della **semplicità**!</em>{{<emo "😁️">}}

{{<box info>}}
Più info sul mio lavoro nella [Home](/). Più info su di me nella pagina [Chi sono](/chi-sono/).
{{</box>}}

## Come collaborare

L'idea di base è conoscerci, confrontarci e creare una rete professionale.

Potremmo consigliarci reciprocamente a quei clienti che non possiamo servire in prima persona o magari persino barattare le nostre rispettive competenze.

Dopotutto, per citare [<cite>I promessi sposi</cite> (capitolo 14)](https://it.wikisource.org/wiki/I_promessi_sposi_(1840)/Capitolo_XIV): "una mano lava l'altra, e tutte e due lavano il viso. Non siamo obbligati a far servizio al prossimo?"

<em>Valuto ogni forma di collaborazione... tranne proposte indecenti, tipo quelle di matrimonio. <cite>I promessi sposi</cite> possiamo lasciarli alla letteratura!</em>{{<emo "😜️">}}

### Esempi

Ci sono almeno tre presupposti per poter collaborare:

1. Facciamo lavori sinergici.
2. Offriamo servizi simili, ma per target diversi.
3. Lavoriamo in settori diversi, ma abbiamo lo stesso target.

Se ti interessa, ecco qualche esempio pratico per capirci meglio.

{{<details "### 1. Grafica, sviluppo web e marketing digitale">}}

Con i miei clienti lavoro soprattutto sugli aspetti comunicativi, tra cui: 

* Pianificazione strategica della loro presenza online.
* Scrittura di testi ben strutturati e formattati per il web.
* Scelta di strumenti più adatti alle loro preferenze e capacità.

Tra le mille attività legate al web di cui invece non mi occupo, ci sono:

* Visual/brand identity, loghi, restyling e grafiche assortite.
* Redesign con funzionalità personalizzate e integrazioni varie.
* Campagne pubblicitarie sui motori di ricerca o sui social media.

Se offriamo servizi utili ai nostri rispettivi clienti, perché non collaborare?
{{</details>}}

{{<details "### 2. Consulenza web per aziende o enti pubblici">}}

Ciao collega! Tu sì che hai scelto bene: le aziende hanno _i big money_.

Io invece preferisco aiutare i freelance _povery_ come me, specie chi lavora da:

* Nomade digitale.
* Artista indipendente.
* Professionista intraprendente.

Scherzi a parte, mi piace aiutare gente creativa anche un po' anticonvenzionale.

Non fa proprio per me il modo di lavorare serioso, formale e impersonale tipico di aziende ed enti pubblici (o anche di libere professioni svolte "all'antica").

Non mi sembra ci sia niente di male nello scegliersi i propri clienti... ma perché non passare quelli con cui non lavoriamo a chi saprà servirli al meglio?
{{</details>}}

{{<details "### 3. Coaching di varia natura e servizi collaterali">}}

C'è una vagonata di roba che può essere utile a chi lavora e comunica online!

Queste sono le prime tipologie di servizi che mi vengono in mente:

* Accountability coaching, business design, consulenza finanziaria.
* Illustrazioni, animazioni, montaggio video, ritocco fotografico.
* Traduzioni, corsi di lingua o dizione, language coaching.
* Supporto psicologico, counseling, life coaching.

Sicuramente c'è molto altro a cui non ho pensato, ma spero di aver reso l'idea.

Non è certo un dovere aiutare i nostri clienti oltre ciò che possiamo offrire loro... ma perché non farlo, se basta una semplice raccomandazione al volo?
{{</details>}}

### Parliamone

Ti piace l'idea di fare rete? [Contattami](/contatti/) e facciamoci una bella chiacchierata!{{<emo "😎️">}}

---

{{<q a="Pink Floyd" w="Hey You" u="https://www.youtube.com/watch?v=c-MU_5VkjtE">}}
_Together we stand, divided we fall_
{{</q>}}
