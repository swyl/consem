---
title: "Quotes & Sources"
description: "The sources of all the quotes I used on my Homepage, plus some extras. P.S. Simplicity can save you money, time, and energy. Ask me how! 🚀️"
layout: en
---

As you may have noticed back in my [Home page](/en/), I quite enjoy quotes. And I like 'em properly referenced, not to spread misinformation (alongside great ideas).

Sources can help find even more interesting thoughts. Links make this process much easier, but they may take focus away and disrupt the flow of the page.

So, I decided to gather in here all the relevant material – and then some.

(This also goes to show you don't need to strip your site down to the bare minimum, if you get your priorities right. Otherwise, my home page would have been just three senteces long – or only one!)
{.small-text}

Without further ado, let's go over all the quotes (in order of appearance).

## Jon Jandai

He gave a TED<sup>x</sup> talk titled [<cite>Life is easy. Why do we make it so hard?</cite>](https://www.youtube.com/watch?v=21j_OCNLuYg) (in 2011 at Doi Suthep, Thailand). It's an ode to simplicity – and one of my all-time faves.

> When I work hard, why is my life so hard? There must be something wrong, because I produce a lot of things, but I cannot get enough.

## Morpheus

Well, [<cite>The Matrix</cite>](https://en.wikipedia.org/wiki/The_Matrix) is a classic. Morpheus is a quintessential coach and mentor, akin to Dante's Virgil. And he has such a laid-back, confident, cool demeanor.

Here's the video clip where [Morpheus shows the door to Neo](https://www.youtube.com/watch?v=xc3VG9JZM6I). I hope YouTube doesn't take it down due to copyright. (See? That's why I'm all for [free culture](https://freedomdefined.org/Definition)!)

> I'm trying to free your mind, Neo. But I can only show you the door.
> 
> You're the one that has to walk through it.

## Henry David Thoreau

Oh, man! This guy is a tresure trove. The quote I've chosen comes from [<cite>Walden</cite>](https://www.walden.org/work/walden/), which you can read in full (and for free) thanks to the Walden Woods Project.

There's also a collection of [Thoreau's quotes about simplicity](https://www.walden.org/quotation-category/morals-and-values/simplicity/) – the one I picked is about the end of page 2, and it comes with some more context.

P.S. Here's a few extra words of wisdom just for you:

* "The cost of a thing is the amount of what I will call life which is required to be exchanged for it, immediately or in the long run."
* "It is not enough to be industrious; so are the ants. What are you industrious about?"
* "The question is not what you look at, but what you see."

> Our life is frittered away by detail. \[...] Simplicity, simplicity, simplicity! I say, let your affairs be as two or three, and not a hundred or a thousand \[...]

## William of Ockham

This is the philosopher who went down in history for [Occam's razor](https://en.wikipedia.org/wiki/Occam%27s_razor), which is basically a logical principle that advocates simplicity and parsimony.

Truth be told, I wanted to quote Albert Einstein at first – but it's always hard to navigate through misattributions and all the shamelessly made up nonsense.

Luckily, something to the effect of "[Everything should be made as simple as possible, but not simpler](https://quoteinvestigator.com/2011/05/13/einstein-simple/)" had already been written a few centuries back.

Wikisource has the full text of [<cite>Summae Logicae</cite> – in Latin](https://la.wikisource.org/wiki/Summa_logicae). Or, you may read a parallel translation on the Logic Museum wiki: [<cite>Sum of Logic</cite>, Book I, Chapter 12](https://www.logicmuseum.com/wiki/Authors/Ockham/Summa_Logicae/Book_I/Chapter_12).

Trivia: [Ockham's razor is a modern myth](https://www.logicmuseum.com/authors/other/mythofockham.htm), but nevermind – the principle is sound.

> There's no point in doing with more what can be done with less. <em lang="la" class="secondary-text">[frustra fit per plura quod potest fieri per pauciora]</em>

## Edsger Wybe Dijkstra

He was a pioneer in Computer Science, and brilliant at that. In 1984, he wrote an essay cheering for elegance and praising simplicity – too often underrated.

[<cite>On the nature of Computing Science</cite>](https://www.cs.utexas.edu/users/EWD/transcriptions/EWD08xx/EWD896.html) takes just a few minutes to read. And it's still quite relevant today, since many people wrongly conflate simple and easy.

Fourteen years later, none other than Steve Jobs said "[simple can be harder than complex](https://allaboutstevejobs.com/verbatim/quotes)" and once you simplify your thinking "you can move mountains".

To me, [Bruno Munari](https://en.wikipedia.org/wiki/Bruno_Munari) had already put it much more beautifully six years back in <cite lang="it">Verbale Scritto</cite> (literally "Written Minutes" – sadly only published in Italian).

> Simplicity is a great virtue but it requires hard work to achieve it and education to appreciate it. And to make matters worse: complexity sells better.

## Antoine de Saint Exupéry

He is mostly renowned for [<cite>The Little Prince</cite>](https://en.wikiquote.org/wiki/The_Little_Prince) – "Here's my secret. It's very simple: one can only see clearly with the heart. The essential is invisible to the eyes."

Instead, I cited an excerpt of [<cite>Wind, Sand and Stars</cite>](https://en.wikiquote.org/wiki/Antoine_de_Saint_Exup%C3%A9ry#Terre_des_Hommes_(1939)) (although I stuck with a more literal rendition than the official translation into English by Lewis Galantière).

> It seems that perfection is attained not when there is nothing more to add, but when there is nothing more to remove. <em lang="fr" class="secondary-text">[Il semble que la perfection soit atteinte non quand il n'y a plus rien à ajouter, mais quand il n'y a plus rien à retrancher.]</em>

## Leo Babauta

He's the author of the popular blog [zen habits](https://zenhabits.net/), launched in 2007.

I cited the opening of [<cite>stop making it complicated</cite>](https://mnmlist.com/stop-making-it-complicated/) from mnmlist, his other blog specifically about minimalism – apparently only active between 2009 and 2017.

If you enjoy simplicity, you'll probably resonate with much of Leo's writings.

> Now that I've learned to look at things with the lens of simplicity, I can see others making mistakes I've made in the past. I want to gently say to them — and to my past self — "Stop making things so complicated!"

## William Shakespeare

Initially, I had reserved this space for another English poet (albeit decidedly less acclaimed). Enter [Robert Southey](https://en.wikiquote.org/wiki/Robert_Southey) and his own maxim on brevity.

"If you would be pungent, be brief; for it is with words as with sunbeams – the more they are condensed, the deeper they burn."

Fittingly enough, I first read this metaphor in [<cite>the art of brief emails</cite>](https://mnmlist.com/the-art-of-brief-emails/) – an article from Leo Babauta's blog I've referenced just a few paragraphs above.

Acting on the very principle hinted at by the quote itself, I thought I'd keep the [Contact](/en/#contact) section as brief as possible. And the rest is history.

"If this be error and upon me proved,  
I never writ, nor no man ever loved."

[<cite>Sonnet 116</cite>](https://en.wikipedia.org/wiki/Sonnet_116) aside, and back to the <cite>Hamlet</cite> – 'twas [Lord Polonius whom I quoth](https://shakespeare.mit.edu/hamlet/full.html#2.2.96). Ironically, he was rather verbose while purporting to be brief. So much so that Queen Gertrude laconically quipped back at him: "More matter, with less art."

(Even more ironically, this turned out to be the longest section. I should have payed heed to Sir Q's advice. Y'know, "[Murder your darlings](https://www.bartleby.com/lit-hub/on-the-art-of-writing/wednesday-january-28-1914/)" and all that jazz.)

> Brevity is the soul of wit.

## Eugène Ionesco

I was **extremely** tempted to just resort to the [Answer to the Ultimate Question to Life, the Universe, and Everything](https://simple.wikipedia.org/wiki/42_(answer)). But that's a darling I indeed murdered.

(It felt a bit off. My [personal blog](https://42m.me) is a much better place for such tomfoolery.)

I wanted a quote that could encourage people to actually ask questions, not to feel either amused or puzzled – based on whether they got the nerd reference.

So I went for this one, that was even displayed on the cover of the first edition of <cite lang="fr">Découvertes: Les sentiers de la création</cite> ("Discoveries: The paths of creation").

I liked the wording cited at the beginning of chapter 3 in <cite>Choosing the Future: The Power of Strategic Thinking</cite> by Stuart Wells – and I reported it verbatim.

> It is not the answer that enlightens, but the question. <em lang="fr" class="secondary-text">[ce n'est pas la réponse qui éclaire, c'est la question.]</em>

## Pink Floyd

Do they even need an introduction? Go listen to [Hey You](https://www.youtube.com/watch?v=c-MU_5VkjtE)!

> Hey you  
> Don't tell me there's no hope at all  
> Together we stand, divided we fall

## Edward Snowden

Privacy and cybersecurity geeks definitely know him already. In 2013, he leaked classified information from the NSA – the National Security Agency of the USA.

Unless you don't care about civil liberties in the slightest, you should be vaguely bothered by mass surveilance. Just, don't go too far down that rabbit hole.

For reference, here's the full [reddit comment on /r/IAmA](https://www.reddit.com/r/IAmA/comments/36ru89/just_days_left_to_kill_mass_surveillance_under/crglgh2/) I took a snippet from.

(You can also watch [<cite>Why privacy matters</cite>, a TED talk by Glenn Greenwald](https://www.ted.com/talks/glenn_greenwald_why_privacy_matters) – one of the first journalists contacted by Edward Snowden.)

> Arguing that you don't care about the right to privacy because you have nothing to hide is no different than saying you don't care about free speech because you have nothing to say. A free press benefits more than just those who read the paper.

## Hernando Fuentes

He's a character from <cite>Sense8</cite> – yet another appearance for [the Wachowskis](https://en.wikipedia.org/wiki/The_Wachowskis).

I was impressed by this line as soon as I first heard it. It's from <cite>Death doesn't let you say goodbye</cite> (Episode 1, Season 9). And it's perfect to endorse [free culture](https://en.wikipedia.org/wiki/Free-culture_movement).

The scene where [Hernando talks about love with Lito](https://www.youtube.com/watch?v=7pwIUXqsV84) is on YouTube. But again, I hope they won't take it down due to copyright – that'd be quite ironic, right?

> Love, like art... must always be free.

## Albert Einstein

Remember I ended up choosing to mention William of Ockham instead of him?

Well, I still wanted to somehow cite uncle Albert. And I was struck by a sudden inspiration. You know his famous (yet disputed) "[Two things are infinite](https://quoteinvestigator.com/2010/05/04/universe-einstein/)" quote?

So yeah, I finally joined the vast chorus of those who ascribe their bullshit to well-known people. To my credit, I only did it to drive my last point home.

> Two internet things are fake: unsourced quotes and unverifiable testimonials. And I'm not sure about the quotes!
