---
title: "Servizi"
description: "Posso aiutarti a risparmiare tempo, soldi ed energie. Sono Michele, il Consulente Web della Semplicità di cui non sapevi di aver bisogno! 😁️"
layout: single
---

* [Sito semplice a quattro mani](/sito-semplice/)
  {.big-text style="margin-top: 0;"}

  Ti guido passo passo, da zero a online.
  {.small-text}

* [Coaching della semplicità](/coaching-semplicita/)
  {.big-text style="margin-top: 0;"}

  Per migliorare la tua comunicazione web.
  {.small-text}
{.whyme-list}

{{<box info>}}
Il nostro primo incontro sarà una [consulenza gratuita](/consulenza-web-gratuita/). Dopodiché, ogni percorso sarà individuale e personalizzato. Per qualsiasi altra informazione, [contattami](/contatti/)!
{{</box>}}

## Domande e risposte

{{<details "### Che metodi di pagamento accetti?">}}
Bonifico bancario anticipato, per confermare il lavoro e bloccare le date.

Troverai l'IBAN nella stessa email con la proposta o il preventivo.

Se hai esigenze particolari, troveremo insieme un'alternativa.
{{</details>}}

{{<details "### I prezzi sono IVA inclusa o IVA esclusa?">}}
In realtà sarebbero esenti IVA, come da regime fiscale forfettario.

Comunque sia, si riferiscono all'importo totale da pagare in fattura.

Ogni altro onere fiscale sarà a mio carico, senza addebiti a sorpresa.
{{</details>}}

{{<details "### Posso cambiare o cancellare una prenotazione?">}}
Sì, ti basta [contattarmi](/contatti/) (possibilmente con un preavviso di almeno un giorno).

Possiamo sempre venirci incontro: ritardi e imprevisti capitano a chiunque.

Come in ogni rapporto di fiducia, l'unica regola è il buon senso.
{{</details>}}

---

{{<q a="Tim Berners-Lee" w="Discorso alla Knight Foundation" u="https://webfoundation.org/about/community/knight-2008-tbl-speech/">}}
Il web non connette solo i computer: connette le persone.
{{</q>}}
