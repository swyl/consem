---
title: "Inviami una mail"
seoTitle: "Contatti"
description: "Chiedimi una consulenza gratuita o anche solo un consiglio al volo. Se posso aiutarti, lo farò con piacere! 🤙"
---

<div id="m">
<svg xmlns="http://www.w3.org/2000/svg" viewBox="-2 0 28 24" width="24" height="24" class="fic big" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
ciao@<i>io (dove "io" = </i>michelenuzzolese.com<i>)</i></div>
<script>m=document.getElementById("m");m.removeAttribute("hidden");e="@e$lvijA&qempxs>gmesDqmglipiry~~spiwi2gsq&B@wzk${mhxlA&5iq&$limklxA&5iq&$gpewwA&jmg$fmk&$|qprwA&lxxt>33{{{2{72svk364443wzk&$jmppA&rsri&$wxvsoiA&gyvvirxGspsv&$wxvsoi1{mhxlA&6&$wxvsoi1pmrigetA&vsyrh&$wxvsoi1pmrinsmrA&vsyrh&$zmi{Fs|A&16$4$6<$68&B@texl$hA&Q8$8l5:g525$4$6$2=$6$6z56g4$52512=$616$6L8g1525$41612=1616Z:g415252=16$616~&B@3texlB@tsp}pmri$tsmrxwA&660:$56057$60:&B@3tsp}pmriB@3wzkB$gmesDqmglipiry~~spiwi2gsq@3eB";h="";for(i=0;i<e.length;i++) h+=String.fromCharCode(e.charCodeAt(i) - 4);m.innerHTML=h;</script>

Diamoci del tu, senza troppe cerimonie!

Puoi chiedermi un consiglio al volo, una [consulenza gratuita](/consulenza-web-gratuita), quello che vuoi.

{{<box info>}}
Rispondo a tutto ciò che non sia spam spudorato, maleducazione o cattiveria.

Se non do segni di vita entro qualche giorno, prova a ricontattarmi: per quanto rara, c'è sempre una possibilità che la tua prima email non mi sia mai arrivata.
{{</box>}}

A presto,  
<span style="display: inline-flex; align-items: center;">
  <span>~<em>Michele</em></span><img srcset="/favicon-192.png 2x, /favicon-32.png 1x" src="/favicon-32.png" style="display: inline; margin-left: 6px; vertical-align: -4px;" width="20" height="20" />
</span>

## P.S. Posso aiutarti se...

{{<details "### ...stai spendendo troppo (e non sai perché)">}}
Spesso non serve pagare un canone mensile di assistenza e manutenzione.

Una presenza online _semplice_ costa appena poche decine di euro all'anno.

Se **temi che il tuo sito sia un salasso**, potresti aver ragione. Parliamone!
{{</details>}}

{{<details "### ...stai perdendo più tempo del previsto">}}
Non potendo investire migliaia di euro, hai scelto di creare un _sito fai da te_.

Hai letto che bastavano pochi minuti, ma **dopo un mese sei ancora a zero**.

So bene che significa! Hai un gran bisogno di semplificare. Chiedimi come.
{{</details>}}

{{<details "### ...ti stai stressando inutilmente">}}
Il tuo sito è lento e pesante? _WordPress_ è un casino?

Dedichi tempo e soldi a mille attività, ma i risultati scarseggiano?

Prova un approccio basato sulla **semplicità**. Hai la mia email: usala!{{<emo "😜️">}}
{{</details>}}

---

{{<q "Robert Southey">}}
Le parole sono raggi di sole. Più sono condensate, più a fondo bruciano.
{{</q>}}
