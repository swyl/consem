---
title: "Sito fatto a mano con ❤️"
seoTitle: "Sito fatto a mano con amore"
description: "Informazioni più o meno tecniche su come ho creato e come gestisco questo sito, tra tecnologie e filosofie varie. 🚀️"
---

Prima di tutto, complimenti per aver trovato questa pagina semi-nascosta. E grazie per aver seguito fino all'ultimo link del sito!{{<emo "🍀️">}}
{.big-text}

<!-- much nerd luv 4 u ❤︎ -->

Qui troverai informazioni su [tecnologie](#tecnologie) usate e [filosofie](#filosofie) adottate.

## Tecnologie

Mantenere questo sito online **mi costa solo 10 € all'anno**.

Perché sono un po' nerd e quindi invece di WordPress, uso:

* [Markdown](https://commonmark.org/), per scrivere i miei testi web con un qualsiasi editor.
* [Hugo](https://gohugo.io/), il mio SSG di fiducia, per montare il sito _pezzo pezzo_.
* [GitLab](https://gitlab.com/), per ospitare il codice sorgente (ecco il mio [repo](https://gitlab.com/swyl/consem)).
* [Netlify](https://www.netlify.com/), per distribuire questa pagina online _aggratisse_.
* [Porkbun](https://porkbun.com), per gestire il dominio e _grugnire alla luna_.{{<emo "🐽️">}}

Poi, dacché ho le mani bucate, spendo pure altri _ben 12 €/anno_ per l'email. 

{{<box info>}}
Al momento uso [Tutanota](https://tutanota.com/), ma sto pensando di cambiare.

Forse lo farò nel _duemilamai_. Anzi, nel duemila**mail**!

_Ok, basta, la smetto._ PER ORA.
{{</box>}}

### Altre robe tecniche

Ho scritto i layout da zero (con HTML/CSS e il linguaggio di template di Hugo, basato su Go). Non uso temi preconfezionati, per amor di efficienza e frugalità.

Inoltre, preferisco usare soltanto codice che capisco, per poterlo "aggiustare" più facilmente se necessario (e sappiamo tutti che prima o poi lo sarà).

Le (poche) icone SVG vengono per lo più da [Feather Icons](https://feathericons.com/) (con qualche ritocco).

---

Evito il più possibile i comportamenti inquietanti del web moderno (tracking, pubblicità, [deceptive design](https://www.deceptive.design/) e richieste assillanti di vario tipo).

JavaScript lo uso poco e responsabilmente, senza rovinare la navigazione alle altre anime folli come me che lo tengono disabilitato di default.

<noscript>
{{<box info "Ma ciao! ❤︎">}}

Se stai leggendo queste righe, allora forse anche tu sei un'_anima folle come me_.

Nella remota ipotesi che tu non abbia idea di cosa sta succedendo qui, potrebbero tornarti utili queste <a href="https://www.enable-javascript.com/it/">istruzioni su come abilitare JavaScript nel tuo browser</a>.
{{</box>}}
</noscript>

Cerco sempre di evitare servizi e prodotti che non rispettano le persone e l'ambiente, per questo prediligo il [software libero](https://it.wikipedia.org/wiki/Software_libero).

---

Non uso Google Analytics. In realtà non uso analytics in generale. Potrei considerare [GoatCounter](https://www.goatcounter.com/), ma per ora l'idea non mi sconfinfera più di tanto.

Non faccio _affiliate marketing_, almeno per il momento. Non ho niente contro le _affiliazioni_, ma aborro le _marchette_. Linko solo ciò che ritengo opportuno.

Evito di dilungarmi oltre, ma se hai qualche domanda [contattami](/contatti) pure.

### Extra: 3 belle cose magiche

Non c'entrano direttamente con questo sito, ma le consiglio con gioia:

* [uBlock Origin](https://github.com/gorhill/uBlock/) – Content Blocker (Raymond Hill santo subito).
* [Bitwarden](https://bitwarden.com/) – Password Manager (ormai non potrei più vivere senza).
* [AnonAddy](https://anonaddy.com/) – Email Forwarding (amo troppo gli alias, ho un problema).

Non so se te ne stai rendendo conto, ma la piccola creatura nerd che vive in fondo al tuo cuoricino starà senz'altro sussultando in segno di approvazione.

## Filosofie

In questa sezione ho raccolto strategie e risorse che mi ispirano e mi guidano. Possono essere utili a chiunque, soprattutto a persone artistiche e creative.

_P.S._ I link sono in inglese, ma per 'sta roba vale la pena impararlo anche se non spiccichi una parola.
{.small-text}

### Progettazione

* [Ethical Design](https://ind.ie/ethical-design/) – <span style="display: inline-block;" aria-hidden="true">▲❤</span> il rispetto alla base, ma anche in cima.
* [Inclusive Design](https://www.nngroup.com/articles/inclusive-design/) – per chiunque, senza discriminazioni.
* [Privacy & Security By Design](https://thenewoil.org/en/guides/prologue/why/) – di serie, non optional.

{{<q a="Aral Balkan" w="Privacy, Data, Democracy" d="The Conference, 2015 – min 8:05" u="https://videos.theconference.se/aral-balkan-privacy-data-democracy">}}
La maggior parte delle volte, la progettazione non fallisce perché non sappiamo risolvere bene un problema, ma perché stiamo risolvendo il problema sbagliato.
{{</q>}}

### Comunicazione

* [World-Class Wanker Loophole](https://www.timminchin.com/2019/03/29/waapa-speech/) – per esprimere la propria unicità.
* [Made-Up-Award Principle](https://robertheaton.com/2019/09/24/how-to-come-up-with-blog-post-ideas/) – anche basta con le brutte copie.
* [Murder Your Darlings](https://www.bartleby.com/190/12.html) – se non è funzionale, via dal testo.

{{<q a="Tim Minchin" w="Occasional Address" d="9 Life Lessons – min 8:54" u="https://www.timminchin.com/2013/09/25/occasional-address/">}}
Anche se non sei un insegnante, _sii un insegnante_. Condividi le tue idee. Non dare per scontata la tua istruzione. Gioisci di quello che impari... e sprizzalo dovunque.
{{</q>}}

### Sviluppo

* [Progressive Enhancement](https://en.wikipedia.org/wiki/Progressive_enhancement) – prima l'_essenziale_, poi il resto.
* [Ship Early, Ship Often](https://en.wikipedia.org/wiki/Release_early,_release_often) – senza naufragare nel _superfluo_.
* [Worse Is Better](https://en.wikipedia.org/wiki/Worse_is_better) – perché _meno_ può essere _più_.

{{<q a="Derek Sivers" w="Version 0.1 = Start lo-fi" u="https://sive.rs/lofi">}}
Se dici che vuoi fare qualcosa, fallo! Non dare la colpa a cause esterne che ti bloccano. Trova il modo di aggirare gli ostacoli e inizia immediatamente.
{{</q>}}

### La vita, l'universo e tutto quanto

* [KISS Principle](https://en.wikipedia.org/wiki/KISS_principle) – _la vie c'est fantastique, pourquoi tu te la complique?_
* [Mental Models](https://fs.blog/mental-models/) – più di 90 in un solo link, perché mi piace vincere facile.
* [Least Dangerous Assumption](https://en.wikipedia.org/wiki/Least_dangerous_assumption) – magari l'empatia ci salverà, non si sa mai.

{{<q a="Leo Babauta" w="Stop Making It Complicated" u="https://mnmlist.com/stop-making-it-complicated/">}}
Ora che ho imparato a guardare le cose con la lente della semplicità, riesco a vedere altre persone fare gli errori che ho fatto in passato.

A loro – e al me stesso del passato – vorrei dire affettuosamente: "Smettila di rendere le cose così complicate!"
{{</q>}}
