---
title: ""
description: "Book a FREE CALL to streamline your web presence and SAVE TIME, MONEY, AND ENERGY. I am Mick, the Simplicity Web Coach you didn't know you needed! 😁️"
layout: en
---

<img srcset="/img/me-intervista-daniele-1200.jpg 4x, /img/me-intervista-daniele-900.jpg 3x, /img/me-intervista-daniele-600.jpg 2x, /img/me-intervista-daniele-300.jpg 1x" src="/img/me-intervista-daniele-300.jpg" alt="Me in a video interview, with a small lavalier mic on my shirt and a big smile on my face." width="300" height="300" class="img-squircle">

I help **freelancers** and **solopreneurs** simplify their web presence.
{.emphasis}

A **simple** website will save you **time**, **money**, and **energy**.
{.emphasis}

{{<cta-btn "#contact" "Contact me now for a free call" "Or read on for more information.">}}

{{<q a="Jon Jandai" w="Life is easy. Why do we make it so hard?" d="TEDxDoiSuthep">}}
When I work hard, why is my life so hard? There must be something wrong, because I produce a lot of things, but I cannot get enough.
{{</q>}}

## About

I work under the **core assumptions** that a professional website:

* Is a means of communication with actual people, before anything else.
* Must be useful to your customers, so that they will _want_ to contact you.
* Should take you as little time and energy as possible to manage (if at all).

In this page you will find information on how I can help you create and run **your own simple website**, with all that matters and **without unnecessary hassle**.

You won't have to deal with technical stuff (unless you want to). We'll focus on a **clear, concise, and credible explanation of the value of your services**.

<nav>
{{<details "### Table of contents">}}
<ul>
  <li><a href="#about">About</a>
    <ul>
      <li><a href="#about-me">About me</a></li>
      <li><a href="#about-you">About you</a></li>
    </ul>
  </li>
  <li><a href="#services">Services</a>
    <ul>
      <li><a href="#simple-website-together">Simple website together</a></li>
      <li><a href="#simplicity-coaching">Simplicity coaching</a></li>
      <li><a href="#one-off-consult">One-off consult</a></li>
    </ul>
  </li>
  <li><a href="#contact">Contact</a></li>
  <li><a href="#appendix">Appendix</a>
    <ul>
      <li><a href="#faq">FAQ</a></li>
      <li><a href="#partnerships">Partnerships</a></li>
      <li><a href="#privacy">Privacy</a></li>
      <li><a href="#license">License</a></li>
      <li><a href="#sources">Sources</a></li>
    </ul>
  </li>
</ul>
{{</details>}}
</nav>

### About me

I am a curious, easy-going, and positive person. My actual name is _Michele_, but you can call me _Mick_. I hope that's easier to remember and harder to misspell.

I'm passionate about how people communicate and interact online. I made my first website when I was 12, self-taught. I have a degree in Computer Science.

I've become a _simplicity web coach_ to help people apply the principles and best practices of online communication, without having to study them for years.

{{<q a="Morpheus" w="The Matrix">}}
I can only show you the door. You're the one that has to walk through it.
{{</q>}}

### About you

You care about **quality**. You work with **passion**. You want to make a positive impact in the world, however little.

You value **authenticity** and respect. You want to manage your communication in full **autonomy**. You don't want to be at anyone's mercy.

You embrace **simplicity**. You are aware of the importance of **peace of mind**. You are willing to eliminate the superfluous and focus on the essential.

{{<q a="Henry David Thoreau" w="Walden" d="chapter 2: Where I Lived, and What I Lived For">}}
Our life is frittered away by detail.
{{</q>}}

## Services

Our **first meeting** is always **free of charge**. We will get to know each other, go over your online presence, and find simple solutions to streamline it.

Then, if needed, we can:

* Make a [simple website together](#simple-website-together) that will help you get new customers.
* Take on a [simplicity coaching](#simplicity-coaching) to improve your web-related workflow.
* Arrange a [one-off consult](#one-off-consult) to get expert advice on your situation.

{{<q a="William of Ockham" w="Summa logicae" d="Pars I: De terminis">}}
There's no point in doing with more what can be done with less.
{{</q>}}

### Simple website together

As professionals, we must communicate with potential customers. But instead of repeating ourselves over and over again, we can just point them to our site.

You don't need anything fancy or expensive. This web page only costs me about 10 euros per year to keep online, and it allows me to communicate with you.

In this coaching program, we will:

1. Focus on your potential customers and why they should contact you.
2. Craft a simple and clear introduction about you and your services.
3. Turn that introduction into a simple, clear, professional website.

I'll have to assess your situation for an accurate estimate, but approximately:

* A **one-page website** takes about a week and 500 euros.
* A **multi-page website** can take up to a month and 1500 euros.
* Any **custom solution** should range roughly from 200 to 2000 euros.

{{<cta-btn "#contact" "Book your free session now" "Or read on for more information.">}}

{{<box info>}}
During our first free meeting, I will help you:

* Understand _if_ and _how_ a website can simplify your workflow.
* Evaluate your business goals, according to your needs and wants.
* Assess several options, whether they involve working together or not.
{{</box>}}

{{<box warning>}}
You could also do it yourself, but then your website may (and likely will):

* Take you waaaay more time (and frustration!) to make.
* Cost you more money to keep online (and need more maintenance).
* Harm your professional image, due to technical and strategical mistakes.

{{</box>}}

{{<q a="Edsger Wybe Dijkstra" w="On the nature of Computing Science" d="EWD896">}}
Simplicity is a great virtue but it requires hard work to achieve it and education to appreciate it. And to make matters worse: complexity sells better.
{{</q>}}

### Simplicity coaching

Freelancers and solopreneurs rarely need more than a simple online presence, which may take as little as 10 euros per year and virtually zero maintenance.

Despite this, a typical professional website is likely packed with features you don't really need and your customers couldn't care less about.

As if wasting your time and money wasn't bad enough, useless resources often also degrade your online communication and harm your professional image.

If that's the case for you, in this tailor-made coaching program we can:

1. Uncover and understand your real needs, to cut waste of time and money.
2. Identify and remove the needless, to make your site faster and clearer.
3. Prioritize and add the needful, to win your potential customers' trust.

I can't give you an accurate estimate before evaluating your situation, but:

* We will start with a **free check-up** of your site and online workflow.
* You can get a **weekly session + email support** for 350 euros per month.
* We can work out a **fully custom coaching program** to fix specific issues.

{{<cta-btn "#contact" "Book your free session now" "Or read on for more information.">}}

{{<box info>}}
During our first free meeting, I will help you:

* Evaluate your current workflow and business goals.
* Identify the most critical issues of your site.
* Discuss solutions to simplify your work.
{{</box>}}

{{<box warning>}}
If you choose to hire a web professional or go full-on DIY, keep in mind that:

* It takes good information to make good decisions (I can help you there).
* To tell a good job from a bad one, you may have to be an expert yourself.
* The more you do, the more you risk making mistakes (without even noticing).
{{</box>}}

{{<q a="Antoine de Saint Exupéry" w="Terre des Hommes" d="chapter III: L'Avion">}}
It seems that perfection is attained not when there is nothing more to add, but when there is nothing more to remove.
{{</q>}}

### One-off consult

Life is complex and work can get messy, but everything can be simplified if you let go of energy-draining distractions and focus on what truly matters to you.

An approach based on simplicity can help you:

* Get rid of the biggest struggles in your workflow.
* Reclaim time and peace of mind for yourself.
* Achieve better results with less effort.

I'm up for one-time web consulting sessions to:

* Clear up your doubts through proven best practices and domain expertise.
* Empower you with actionable knowledge and simple solutions.
* Help you make the best possible decisions for yourself.

A consultation with me:

* Costs 75 euros.
* Has an informal, relaxed, and friendly vibe.
* Lasts as long as it takes (generally around the 1-hour mark).

The idea is: the fee you pay is for my skills. Time is too valuable to be sold, so I gladly give it away. Speaking of, our first meeting is free of charge!

{{<cta-btn "#contact" "Book your free session now" "Or read on for more information.">}}

{{<box info>}}
Over a consulting session I can help you:

* Understand what to look out for when you need to buy a web service.
* Choose platforms or tools based on your skills and preferences.
* Come up with a master plan to accomplish a meaningful goal.

After that, keep in mind that a custom coaching program may also help you:

* Hold yourself accountable, to boost your productivity.
* Curb your perfectionism, to be less worried and stressed.
* Learn new skills, to become more efficient and independent.
{{</box>}}

{{<box warning>}}
Before you needlessly squander your time, I would like to remind you that:

* Anything is easy, if you know how to do it. Else, it can be utterly painful.
* In the era of information overload, filtering out bad information is key.
* Working by yourself may be tough. Together, everything gets simpler.
{{</box>}}

{{<q a="Leo Babauta" w="Stop Making It Complicated">}}
Now that I've learned to look at things with the lens of simplicity, I can see others making mistakes I've made in the past. I want to gently say to them — and to my past self — "Stop making things so complicated!"
{{</q>}}

## Contact

Drop me a line to ask for more information, get quick advice, or book a free call.

<div id="m">
<svg xmlns="http://www.w3.org/2000/svg" viewBox="-2 0 28 24" width="24" height="24" class="fic big" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
ciao@<i>me (where "me" = </i>michelenuzzolese.com<i>)</i></div>
<script>m=document.getElementById("m");m.removeAttribute("hidden");e="@e$lvijA&qempxs>gmesDqmglipiry~~spiwi2gsq&B@wzk${mhxlA&5iq&$limklxA&5iq&$gpewwA&jmg$fmk&$|qprwA&lxxt>33{{{2{72svk364443wzk&$jmppA&rsri&$wxvsoiA&gyvvirxGspsv&$wxvsoi1{mhxlA&6&$wxvsoi1pmrigetA&vsyrh&$wxvsoi1pmrinsmrA&vsyrh&$zmi{Fs|A&16$4$6<$68&B@texl$hA&Q8$8l5:g525$4$6$2=$6$6z56g4$52512=$616$6L8g1525$41612=1616Z:g415252=16$616~&B@3texlB@tsp}pmri$tsmrxwA&660:$56057$60:&B@3tsp}pmriB@3wzkB$gmesDqmglipiry~~spiwi2gsq@3eB";h="";for(i=0;i<e.length;i++) h+=String.fromCharCode(e.charCodeAt(i) - 4);m.innerHTML=h;</script>

{{<box info>}}
If I don't reply within a few days, try contacting me again: I may have missed your first email – or delivery may have failed due to network errors, spam filters, etc.
{{</box>}}

{{<q a="William Shakespeare" w="Hamlet" d="act 2, scene 2">}}
Brevity is the soul of wit.
{{</q>}}

## Appendix

### FAQ

I can't anticipate any and all of your questions, so please let me know if I missed something important to you – or if you're still left with any doubts.

{{<details "#### How does our free call work?">}}
Just email me, introduce yourself, and mention your website (if you have one).

We'll arrange the details, and I'll send you a link to join 10 minutes before the meeting. It should take us roughly 30 minutes, but I am flexible about that.

Powered by Jitsi Meet: a free, secure (open source and end-to-end encrypted), intuitive video conferencing tool. It works from any device, no account needed.
{{</details>}}

{{<details "#### Why are all your prices in euros?">}}
Because I am currently based in Italy. But we'll find a way for you to pay in your local currency. This is my top priority at the moment.

I am still in the process of evaluating options to make payments easier and cheaper for both of us. If you have anything to suggest, please let me know.

Meanwhile, I take this chance to clarify that transaction fees and any other silly Italian fiscal burdens are on me. No surprise charges on the invoice.
{{</details>}}

{{<details "#### Do I really need a website?">}}
No, or else you wouldn't even ask. Some don't have one, and they do just fine. People telling you otherwise have either a vested interest or a biased opinion.

That said, your website is the only place you can fully control on the internet. And it can become a great business tool, if you know how to use it (and why).

Not to mention that most people expect any legitimate business to have a web presence, which affects your professional image (for better or for worse).
{{</details>}}

{{<details "#### Can't I just use social media?">}}
Sure you can, but your website serves a different purpose – first and foremost, introducing yourself professionally to new potential customers.

Social media are ephemeral by design, but they can help you keep in touch with people you already know (though the consensus is newsletters work best).

Most importantly, you depend on external companies. They may fold, change terms of service, or ban you for no reason. You can lose everything anytime.
{{</details>}}

{{<details "#### Can you help me with... (insert issue here)?">}}
Well, you'll never know until you ask. Maybe we can work something out.

Problems are there to be solved, and I am here to help you find solutions.

If you have any doubts, just tell me. Things might be easier than you think.
{{</details>}}

Anything else you'd like to know? [Ask away](#contact)!

{{<q a="Eugène Ionesco" w="Découvertes: Les sentiers de la création">}}
It is not the answer that enlightens, but the question.
{{</q>}}

### Partnerships

I'd love to create a network of like-minded professionals on the web who can work in synergy. Here's just a few examples on how we could do that:

* I set up the page structure, you focus on the presentation.
* I make communication clearer, you drive traffic and acquisition.
* I help customers do it themselves, you offer to do it on their behalf.

That's why I'm looking notably for visual designers (well-versed in UX and a11y), non-sleazy marketers, and anyone working for freelancers or solopreneurs.

Also, there are many ways to support each other while helping our customers. We can't cater to everyone, but at least we can leave people in good hands if:

* One of us is temporarily unavailable (e.g. on leave or at full capacity).
* We target a different audience (e.g. you don't work with freelancers).
* A customer needs us both (e.g. to translate and simplify a web page).

If you feel we might agree on professional ethics and a few personal values, I'd be very happy to talk about what you do and how we can possibly partner up.

{{<q a="Pink Floyd" w="Hey You">}}
Together we stand, divided we fall
{{</q>}}

### Privacy

In short: this website doesn't collect any data about you, it doesn't track you, it doesn't spy on you. There's nothing creepy going on in here.

If you don't take my word for it, I'm proud of you for not trusting a stranger on the internet! And I'll tell you why you won't even need to trust me at all.

I will show you a few ways to protect your privacy, regardless of what site owners say. I will only recommend what I use or like – no affiliate bullshit.

#### Your email

I can't possibly know who you are until you contact me to introduce yourself.

After that, I will never use your email to pester you. I treat you as a person, not as a customer. We can politely talk about anything, I won't be a jerk.

If you're concerned I might turn into a jerk at some point, you can always use [addy.io](https://addy.io/) – or create a new junk email inbox, but that's kind of a drag.

#### Your IP

I don't use analytics. I won't ever know you've been here, unless you email me.

Check out [Netlify's Privacy Policy](https://www.netlify.com/privacy/) to find out how they temporarily log your IP.

Or, just use a decent VPN. Avoid shitty uber-advertised ones (such as NordVPN). You can try [Mullvad](https://mullvad.net) or [iVPN](URL) – but if you truly care, you know nothing beats [Tor](https://www.torproject.org/).

#### Anything else

Should I need to rely on a third-party service, I'd always choose Free/Libre and Open Source Software over <del>proprietary crap</del><ins> – I mean, proprietary solutions</ins>.

If you don't know what that implies, let me rephrase it: I can tell what really sucks, and we're in this together. So, you're pretty safe with me.

You can learn the basics of privacy and security on [The New Oil](https://thenewoil.org/). If you're a web pro, check out [Ethical Design](https://ind.ie/ethical-design/) – or at least steer clear of [Deceptive Patterns](https://www.deceptive.design/).

{{<q a="Edward Snowden" w="Reddit" d="comment on /r/IAmA">}}
Arguing that you don't care about the right to privacy because you have nothing to hide is no different than saying you don't care about free speech because you have nothing to say. A free press benefits more than just those who read the paper.
{{</q>}}

### License

Admittedly, there's not much on this page. Why anyone would be interested in copying it is beyond me – but they still can, if they wanted to. Legally.

This website is released under a [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) – as long as you follow the license terms, you can share or adapt anything without even asking.

It's mostly a matter of principle: I care about freedom, so it is only logical for me to contribute to the commons with [Free Cultural Works](https://freedomdefined.org/Definition). Long live free art!

{{<q a="Hernando Fuentes" w="Sense8" d="S01E09">}}
Love, like art... must always be free.
{{</q>}}

### Sources

Wait, are we talking about code or quotes?

In any case, chances are you're a bit of a nerd. High five!

Feel free to check out the source code of this website on my [repo on GitLab](https://gitlab.com/swyl/consem), in case you're into that kind of stuff.

As for the other kind, you can explore all my [quotes sources](/quotes/) (plus some extra).

Point is, everyone can write whatever they want on their site. You should be doubting every word by default. Especially when it comes to web testimonials.

{{<q a="Albert Einstein">}}
Two internet things are fake: unsourced quotes and unverifiable testimonials. And I'm not sure about the quotes!
{{</q>}}
