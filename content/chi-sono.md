---
title: ""
seoTitle: "Chi Sono"
description: "Mi chiamo Michele e sono il tuo Consulente Web della Semplicità preferito... anche perché l'unico che esista! 😁️"
---

<section id="intro">

<div class="introduction-container clearfix">
<img class="propic" srcset="/img/michele-viso-2x.jpg 2x, /img/michele-viso.jpg 1x" src="/img/michele-viso.jpg" alt="" width="160" height="160">
<div class="introduction-txt">

# Piacere, Michele! 👋

Aiuto **freelance** e **piccole attività** a comunicare sul web _in modo semplice e professionale_.
{.big-text}
</div>
</div>
</section>

<section id="come-lavoro">

## Come lavoro

Nuoto **controcorrente** in un mare di consumismo e marketing manipolatorio per rimuovere sprechi di **tempo**, **soldi** ed **energie** dalla tua presenza online.

Il mio approccio si basa su tre pilastri:

* **Strategia efficace**: _piccoli accorgimenti_ che portano _grandi benefici_.
* **Tecnica efficiente**: lavorare _meno e meglio_ per avere _più risultati_.
* **Soluzioni semplici**: con _poco da perdere_ e _tanto da guadagnare_.

{{<box info>}}
Il modo di lavorare tradizionale, serioso e formale non fa proprio per me:

* A enti impersonali e "persone" giuridiche, preferisco le **persone vere**.
* Supporto con piacere la **gente creativa**, anche un po' anticonvenzionale.
* Apprezzo chi ha un'attitudine da **nomade digitale** o **artista indipendente**.

Se vuoi una mano a semplificare, la [consulenza gratuita](/consulenza-web-gratuita/) fa al caso tuo.
{{</box>}}

{{<box ok>}}
In generale, non lavoro _per_ te ma _con_ te. Ad esempio, posso aiutarti a:

* Comunicare con **testi** chiari, strutturati e formattati per il web.
* Scegliere gli **strumenti** più adatti alle tue preferenze e capacità.
* Acquisire **clienti** attraverso un sito di presentazione professionale.

Puoi sempre [contattarmi](/contatti/) in caso di dubbi, proposte o richieste al volo.
{{</box>}}

{{<box error>}}
Per contro, tra le mille attività digitali di cui _non_ mi occupo ci sono:

* **Sviluppo** di funzionalità complesse e integrazioni personalizzate.
* Visual identity, branding, restyling, loghi e **grafiche assortite**.
* **Campagne pubblicitarie** su motori di ricerca o social media.

Offri uno o più di questi servizi e vorresti collaborare? [Facciamo rete](/collaborazioni/)!
{{</box>}}
</section>

<section id="storia">

## Un po' di storia

Sono sempre stato **appassionatamente curioso**.

Ero uno di quei bambini che volevano sapere il perché di tutto. Gli adulti, immancabilmente, riuscivano a rispondermi solo fino ad un certo punto.

Poi ho scoperto internet. Amore a prima vista!{{<emo "😍">}}

La mia sete di conoscenza finalmente incontrava un'oasi.

### Il mio percorso in 3 tappe

1. A **12 anni** ho imparato le basi di [HTML](https://it.wikipedia.org/wiki/HTML) e [JavaScript](https://it.wikipedia.org/wiki/JavaScript) per creare il mio **primo sito**.
    
   Ovviamente era una ciofeca, ma io ero contentissimo di condividere col mondo la mia passione per Final Fantasy VIII!{{<emo "🤣">}}

   Poi sono diventato uno dei primi **blogger** della mia generazione, all'epoca della ormai defunta piattaforma [Splinder](https://it.wikipedia.org/wiki/Splinder).
    
    Da allora, ho aperto e chiuso vari blog. E da ciascuno ho tratto qualcosa di utile.

2. Sul finire delle scuole superiori ho preso un lavoretto part-time.

    Poi ci son rimasto per 8 anni (alla faccia del _lavoretto part-time_), arrivando a gestire un **e-commerce** di articoli sportivi tra i più competitivi del settore.

    Nel frattempo, mi sono laureato in **Informatica**.
    
    Ma quando chiedo di indovinare il mio percorso di studi, i tentativi più frequenti sono **Filosofia**, **Lingue** e **Psicologia**. <em>Chissà cosa vorrà dire!</em>{{<emo "🤔">}}

3. Nel corso degli ultimi anni mi sono appassionato sempre più a tematiche web quali **usabilità**, **accessibilità**, **sicurezza** e **privacy online**.
    
    Sono discipline che ormai riguardano ognuno di noi, a maggior ragione chi vuol prendersi cura in autonomia della propria **immagine professionale** sul web.
{.steps}

### E adesso?

Mi sono autoproclamato _Consulente Web della Semplicità_.

1. _Consulente_, perché elargisco **consigli**, **informazioni** e **formazione**.
2. _Web_, perché lavoro all'incrocio tra **informatica** e **comunicazione**.
3. _Semplicità_, perché il mio vero obiettivo è **semplificarti la vita**.

In una società che ci spinge a comprare il più possibile, io suggerisco di togliere prima che di aggiungere, per lavorare con **meno stress** e **più pace mentale**.
</section>

<section id="valori">

## Valori personali e professionali

> Sogno un web libero per tutti, senza censure o discriminazioni.

Ecco **5 valori** che ispirano le mie **azioni** e definiscono le mie **priorità**.

### 1. Armonia

Sono alla costante ricerca di **equilibrio** tra me, gli altri e l'ambiente in cui viviamo. Voglio creare situazioni in cui vincono _tutti_.

Per me **empatia** e **sostenibilità** (sociale, ecologica ed economica) rientrano in qualsiasi equazione, perché _tutto è collegato_.

Significa rinunciare attivamente a ciò che provoca _danni collaterali_ ed essere sempre animato da **buone intenzioni** verso ogni cosa.

{{<q a="Aurelius Augustinus" w="In Epistulam Ioannis ad Parthos" d="Tractatus VII" u="https://la.wikisource.org/wiki/In_Epistolam_Ioannis_ad_Parthos/7" l="a">}}
<em lang="la">Dilige, et quod vis fac.</em> <span class="secondary-text">[Ama! E fa' ciò che vuoi.]</span>
{{</q>}}

### 2. Autenticità

Credo che ogni persona dovrebbe sentirsi **libera di esprimersi** nel modo più vero alla propria natura. Con **rispetto** per gli altri, ma anche per sé.

Per me questo implica **onestà** (intellettuale e materiale) e amore per la **verità** (oggettiva o soggettiva che sia, in base ai casi).

Significa dire sempre le cose come stanno, anche quando è antieconomico; e comportarmi come sento sia giusto, non come conviene di più.

{{<q a="Bob Cody" w="Interstate 60" u="https://it.wikipedia.org/wiki/Interstate_60" wa="Bob Gale">}}
<em lang="en">Say what you mean, mean what you say.</em>
{{</q>}}

(Giochi di parole a parte, il senso sarebbe: «Di' ciò che pensi davvero e prendi sul serio ciò che dici.»)
{.small-text}

### 3. Correttezza

Non mi piace agire con sufficienza, a prescindere dai vantaggi che potrei trarne. Voglio sentirmi soddisfatto di come decido di impiegare il mio tempo.

Per me è come con la **qualità**: più costosa nell'immediato, ma alla lunga paga. Magari in modi imprevedibili, perché _la vita non è uno sprint: è una maratona_.

Significa **fare la cosa giusta**, anche se comporta più sacrificio: _su alcune posizioni_, rispondo solo alla domanda della _coscienza_.

{{<q c="<b>Martin Luther King Jr.</b> <a href='https://wist.info/king-martin-luther/12678/'>(da un discorso del 14 gennaio 1968)</a>">}}
...e arriva il momento in cui si deve prendere una posizione che non è né sicura, né diplomatica, né popolare; ma _bisogna farlo_, perché la coscienza dice che è giusto.
{{</q>}}

### 4. Positività

Riesco trovare _almeno_ un lato buono in ogni situazione. Mi diverto con poco e mi lascio trasportare dall'**entusiasmo**.

Per me la felicità è una **scelta**: preferisco gioire di quello che ho piuttosto che lamentarmi di quello che mi manca. 

Significa _sorridere alla vita_, perché magari non potrò decidere ciò che mi capita, ma posso sempre decidere **come reagire**.

{{<q a="Ketut" w="Mangia, prega, ama" wa="Elizabeth Gilbert" u="https://it.wikipedia.org/wiki/Mangia,_prega,_ama_-_Una_donna_cerca_la_felicit%C3%A0">}}
Sorridi con faccia, sorridi con mente. Sorridi anche con fegato.
{{</q>}}

### 5. Semplicità

Ho imparato che per trovare **spazio** per ciò che voglio devo prima liberarmi del **peso** di ciò che non mi serve.

Per me è un _antidoto all'entropia_ del mondo. Una **bussola**, un **approccio**, una **bilancia**, una _giusta via di mezzo_.

Significa far luce su quello che reputo davvero importante, per **eliminare il superfluo** e **concentrarmi sull'essenziale**.

{{<q a="Jon Jandai" w="La vita è facile. Perché la facciamo così difficile?"  d="TEDx Doi Suthep" u="https://www.youtube.com/watch?v=21j_OCNLuYg" l="a">}}
Se mi impegno così tanto, perché la mia vita è così difficile? Deve esserci qualcosa di sbagliato...
{{</q>}}
</section>

<section id="attitudini">

## Attitudini, esperienze, hobby

Mi affascinano le **scienze umane**, soprattutto la **comunicazione**. Mi incuriosisce il modo in cui interagiamo, parliamo, pensiamo, percepiamo.

Allo stesso tempo, ho un debole per la **matematica** e le materie tecniche, che secondo me sono ben più _umanistiche_ di quel che sembrano.

Ho un approccio **multidisciplinare**, che mi aiuta a **vedere collegamenti** tra mondi apparentemente distanti e **fare da ponte** tra persone diverse.

Mi piace **aiutare**, perché credo che la cooperazione e la condivisione siano più vantaggiose della competizione a tutti i costi, aggressiva e sfrenata.

---

Amo **esplorare nuovi posti** e forse ancor più **tornarci**. Parlo fluentemente **inglese** e **spagnolo**. A livello basilare, anche _rumeno_ e _bulgaro_.

Mi è sempre piaciuto **imparare**, nel senso più ampio del termine. Ho anche coordinato qualche progetto europeo di **apprendimento non-formale**.

Reputo il **volontariato** fondamentale per la mia formazione umana. Che sia con bambini, ragazzi o adulti; in Italia o fuori; in contesti associativi o meno.

Credo che **logica** ed **emozione** siano complementari e inseparabili. Come **scienza** e **spiritualità** <small>(ma il _dao_ non si può esprimere e di _non-dualismo_ ne parleremo altrove)</small>.

---

Adoro **pensare**, è una delle mie attività preferite. Sono un avido lettore e non perdo occasioni per riflettere [sulla vita, l'universo e tutto quanto](https://it.wikipedia.org/wiki/Risposta_alla_domanda_fondamentale_sulla_vita,_l%27universo_e_tutto_quanto "Spoiler: la risposta è 42.").

Per diletto e vocazione mi do all'**arte**: suono, scrivo, canto, scatto foto, [bloggo](https://42m.me/), contemplo, medito, [faccio cose, vedo gente](https://www.youtube.com/watch?v=vAOsC8zL95E "Nanni Moretti – Ecce Bombo").

Sono [cazzaro alla radice, dovrei piangere ed invece](https://www.youtube.com/watch?v=GaXuMC_GHEE "Caparezza – Jodellavitanonhocapitouncazzo") spargo **citazioni** [dovunque come la rucola](https://www.youtube.com/watch?v=nfD1oQV08Xg "Caparezza – Comunque Dada"). ([_Oops_, l'ho fatto di nuovo!](https://it.wikipedia.org/wiki/Oops!..._I_Did_It_Again_%28singolo%29 "Britney Spears – Oops!... I Did It Again"))

Non so se si nota ([/s](https://www.reddit.com/r/help/comments/acls8e/what_does_s_mean_in_reddit/)), ma ho un senso dell'umorismo **un po' nerd**. Poi non dire che non ti avevo avvisato, eh.
</section>

<section id="fotine">

## Foto

Qualche fotina random non poteva mancare in questa pagina!

Per non romperti lo schermo, ho messo qui solo le anteprime: aprile a tuo rischio e pericolo! 🤣️
{.small-text}

<div class="gallery"><a href="/img/me-foresta-momin-prohod-600.jpg"><figure><img class="img-squircle" srcset="/img/me-foresta-momin-prohod-600.jpg 4x, /img/me-foresta-momin-prohod-450.jpg 3x, /img/me-foresta-momin-prohod-300.jpg 2x, /img/me-foresta-momin-prohod-150.jpg 1x" src="/img/me-foresta-momin-prohod-150.jpg" alt="Camminando in una foresta di pini, con la primavera alle porte." width="150" height="150"><figcaption>Foresta</figcaption></figure></a><a href="/img/me-mansarda-green-house-1200.jpg"><figure><img class="img-squircle" srcset="/img/me-mansarda-green-house-600.jpg 4x, /img/me-mansarda-green-house-450.jpg 3x, /img/me-mansarda-green-house-300.jpg 2x, /img/me-mansarda-green-house-150.jpg 1x" src="/img/me-mansarda-green-house-150.jpg" alt="Facendo ordine in una mansarda in ristrutturazione." width="150" height="150"><figcaption>Ristrutturazioni</figcaption></figure></a><a href="/img/me-intervista-daniele-1200.jpg"><figure><img class="img-squircle" srcset="/img/me-intervista-daniele-600.jpg 4x, /img/me-intervista-daniele-450.jpg 3x, /img/me-intervista-daniele-300.jpg 2x, /img/me-intervista-daniele-150.jpg 1x" src="/img/me-intervista-daniele-150.jpg" alt="Durante un'intervista, con un piccolo microfono lavalier sulla maglietta e un grande sorriso sulla faccia. Qui portavo ancora un dread (una treccina rasta), piuttosto sottile ma abbastanza lungo, con il resto dei capelli a zero." width="150" height="150"><figcaption>Intervista</figcaption></figure></a><a href="/img/me-chitarra-600.jpg"><figure><img class="img-squircle" srcset="/img/me-chitarra-600.jpg 4x, /img/me-chitarra-450.jpg 3x, /img/me-chitarra-300.jpg 2x, /img/me-chitarra-150.jpg 1x" src="/img/me-chitarra-150.jpg" alt="Tanti capelli fa, mentre suonavo una chitarra classica con un'espressione assorta." width="150" height="150"><figcaption>Capelli</figcaption></figure></a></div>
</section>

<section id="ciao">

## Arrivedorci!

Ho cercato di darti una minima idea di che persona sono, ma il modo migliore per scoprirlo resta la tua esperienza diretta: ti basterà solo [contattarmi](/contatti).

Se non ne hai ancora avuto abbastanza dei miei sproloqui, puoi chiedermi una [consulenza gratuita](/consulenza-web-gratuita/) per sentirmi sproloquiare anche in diretta.{{<emo "😜️">}}

<!-- TODO: Se non ne hai ancora avuto abbastanza dei miei sproloqui, puoi dare un'occhiata anche alle altre [risorse gratuite](/risorse) che ho pubblicato in questo sito. -->

Altrimenti, ci vediamo la prossima volta. Per me è sempre un piacere ospitarti in questa casa virtuale. Torna quando vuoi!{{<emo "🥰️">}}

> _Aaa rivedoowrciii!_
{.emphasis}

{{<video "arrivedorci" "Scena tratta dal cortometraggio del 1929 [<cite>Tempo di pic-nic</cite>](https://it.wikipedia.org/wiki/Tempo_di_pic-nic): Stanlio e Ollio salutano il vicinato dall'automobile prima di partire.">}}

---

{{<q a="Antoine de Saint-Exupéry" w="Terra degli uomini">}}
Sembra che la perfezione venga raggiunta non quando non c'è più niente da aggiungere, ma quando non c'è più niente da togliere.
{{</q>}}
</section>
