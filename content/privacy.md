---
title: "Privacy"
description: "La Privacy Policy più semplice del web! 😎️"
---

<section id="intro">

In sintesi:
{.big-text}

* Zero cookie, pubblicità, analytics e plugin vari che ti spiano sul web.{{<emo "😱️">}}
* Non posso stalkerarti se non so chi sei e non ho modo di identificarti.{{<emo "🤔️">}}
* Cerco sempre di usare il buon senso, tranne quando è vietato per legge.{{<emo "🤦">}}

Per me questo sarebbe sufficiente, ma devo scrivere altro per esigenze legali.
</section>

<section id="italiano">

## Informativa breve, in italiano

<em>La Privacy Policy più semplice del web!</em>{{<emo "😎️">}} 

### Che dati personali raccolgo

Fosse per me, nessuno: potremmo comunicare con mail fasulle e pseudonimi.

Solo che poi non posso mica fatturare a _Mastro Geppetto_ o alla _Fata Turchina_.

L'Agenzia delle Entrate del _Paese delle Meraviglie_ si insospettirebbe alquanto.

### Come raccolgo i tuoi dati

Ho una strategia trifasica:

1. Ascolto tutto con attenzione.
2. Aspetto che me li spari in faccia.
3. Se proprio necessario, te li chiedo.

### Come tratto i tuoi dati

Considero tutto confidenzale, semplicemente perché non sono così stronzo da sputtanare la gente (quando non sono vincolato da obblighi fiscali o legali).

Se avrò lo sbatti, archivierò la tua email in un file (rigorosamente criptato).

Per il resto, non ti stresso l'anima e se qualcosa non ti garba, basta dirlo.

### I tuoi diritti

Hai il diritto di rimanere in silenzio. Qualsiasi cosa dirai potrà essere usata contro di te in tribun... ah no, aspe', mi sono confuso.

Hai il diritto di [contattami](/contatti) per qualsiasi domanda, dubbio, informazione, adulazione o anche solo per dirmi quanto sono simpatico da 1 a 42.

Hai il diritto di andare a goderti la vita, oppure di continuare a leggere la pappardella qui sotto (in cui sono indicati anche i tuoi diritti legali).
</section>

<section id="legalese">

## Informativa estesa, in legalese

_Informativa Privacy ai sensi dell'art. 13 del Regolamento UE n. 2016/679 (GDPR)_.

### Glossario

* **Dati personali (o dati)**: qualunque informazione che, direttamente o indirettamente, possa identificare univocamente una persona fisica.
* **Dati di navigazione**: informazioni tecniche tra cui indirizzi IP e orari di connessione, nomi di browser (user agent) o dispositivi utilizzati, etc.
* **Trattamento**: qualsiasi operazione applicata ai dati personali, compiuta con o senza l'ausilio di processi automatizzati.
* **Titolare del trattamento**: la persona fisica che determina finalità e mezzi del trattamento di dati personali (ovvero, in pratica, io).
* **Interessato**: la persona fisica a cui si riferiscono i dati personali (ovvero, in pratica, tu).
* **Base giuridica**: condizioni che legittimano il trattamento dei dati.
* **Finalità**: motivi per cui i dati personali vengono trattati.

### Titolare del trattamento

* **Nome e cognome:** Michele Nuzzolese
* **Partita iva:** 08799350726
* **PEC:** m.n@peceasy.com*

\* Questo indirizzo è configurato per ricevere SOLO comunicazioni provenienti da posta elettronica certificata. Se vuoi contattarmi tramite email ordinaria, vai alla pagina [Contatti](/contatti).
{.small-text}

### Base giuridica del trattamento

Tratto i tuoi dati personali indicati in questa pagina solo per:

* Erogare i servizi da te richiesti
* Rispondere alle tue comunicazioni.
* Adempiere agli obblighi legali e fiscali.

### Tipi di dati e finalità del trattamento

#### Dati forniti liberamente da te

Se mi contatti per email, acquisirò automaticamente i tuoi dati di contatto, necessari per rispondere, nonché eventuali dati inclusi nelle comunicazioni.

Tratto questi dati solo per finalità connesse alle tue richieste, tra cui:

* Attività di consulenza online.
* Risposte alle tue domande specifiche.
* Scambio di pareri, informazioni, suggerimenti.

#### Dati tecnici di navigazione

Personalmente non tratto questi dati in alcun modo per nessuna finalità.

Non essendo proprietario dell'infrastruttura tecnica né dei server che ospitano questo sito, rimando alla [Privacy Policy di Netlify](https://www.netlify.com/privacy/) per ulteriori informazioni.

### Periodo di conservazione dei dati

I dati saranno conservati fino a un massimo di 10 anni dalle nostre ultime comunicazioni e comunque per il tempo di prescrizione previsto dalla legge.

### Mancata comunicazione dei dati

La comunicazione dei tuoi dati personali, trattati in base a questa informativa, è necessaria per usufruire dei servizi da te richiesti attraverso questo sito web.

### Diritti dell'interessato

Hai il diritto di:

* Chiedere l'accesso, la rettifica o la cancellazione dei tuoi dati personali.
* Limitare o opporti al trattamento dei dati personali che ti riguardano.
* Proporre reclamo al [Garante per la protezione dei dati personali](http://www.garanteprivacy.it).

Puoi sempre [contattarmi](/contatti) per qualsiasi altra informazione in merito.
</section>

<section id="outro">

## Conclusione

Se sei ancora qui, forse hai una (mal)sana ossessione per la privacy.

O forse hai aperto questa pagina per caso e scrollato subito verso la fine.

Insomma, delle due l'una:

* Navighi nella più beata ignoranza tecnologica, della privacy online non ti frega una cippa lippa – e quasi quasi ti invidio, perché almeno vivi bene.
* Navighi solo con [Tor](https://it.wikipedia.org/wiki/Tor_(software)) su [Tails OS](https://it.wikipedia.org/wiki/Tails) dai peggiori bar di Caracas a orari random, con [cappuccio d'ordinanza](https://mrrobot.fandom.com/wiki/Elliot_Alderson) – roba che [Edward Snowden](https://it.wikipedia.org/wiki/Edward_Snowden) te spiccia casa.

Io non sarò al _livello Snowden_, ma un paio di cose sulla privacy le ho imparate. Per questo tratto i tuoi dati con la stessa cura che riservo ai miei.

---

{{<q c=`<a href="https://it.wikiquote.org/wiki/Edward_Snowden"><b>Edward Snowden</b> - Wikiquote</a>`>}}
Credere che la **privacy** non ti riguarda perché non hai _niente da nascondere_ è come pensare che la **libertà di parola** non ti riguarda perché non hai _niente da dire_.
{{</q>}}
</section>
