---
draft: true # undraft after adding /risorse
title: "Licenza & Libertà"
description: "Tutti i contenuti originali di questo sito sono liberi, perché sogno un mondo più libero per tutti. 🥰️"
---

Questo sito è distribuito con licenza internazionale [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.it).
{.big-text}

## 'nche senso?

{{<video "nchesenso" "Scena tratta dal film [<cite>Un sacco bello</cite>](https://it.wikipedia.org/wiki/Un_sacco_bello): Leo (Carlo Verdone), visibilmente perplesso, chiede spiegazioni a Marisol (Veronica Miriel).">}}

In soldoni, significa che chiunque può legittimamente usare le mie pubblicazioni originali a qualsiasi fine, anche commerciale, senza bisogno di autorizzazione.

Chiedo solo la cortesia di **citare la fonte**: mi sembra una semplice questione di trasparenza e correttezza, verso chi legge prima ancora che verso di me.

Naturalmente puoi [contattarmi](/contatti) in caso di domande, dubbi, segnalazioni.

### Per evitare abusi...

La licenza che ho scelto _obbliga_ a citare la fonte e _vieta_ la ridistribuzione delle opere protette a condizioni più restrittive del [copyleft](https://copyleft-italia.it/).

La protezione legale è pienamente valida e riconosciuta, ma detto tra noi non ho lo sbatti di mettermi lì a querelare solo per questioni di principio.

Chi invece pensa di lucrare a sbafo, senza voler rilasciare [contenuti liberi](https://it.wikipedia.org/wiki/Contenuto_libero) né riconoscermi un centesimo, sappia che non si può fare. <em>A buon intenditor...</em>{{<emo "🤐️">}}

{{<q a="Paolo Attivissimo" w="Copyright e regole di ripubblicazione" u="https://attivissimo.net/nl/norme_distribuzione.htm">}}
Se non rispettate queste condizioni, la riproduzione dei miei testi è illegale. Pertanto, se mi fate girare le scatole, posso farvi causa e vincere con disinvoltura.

Dunque non fate gli stupidi, anche perché se siete onesti non ce n'è bisogno.
{{</q>}}  

### Perché lo faccio?

Vorrei che le idee, la cultura e la conoscenza siano sempre libere di circolare. Il _copyright_ limita la libertà collettiva, per cui ne faccio volentieri a meno.

Le mie intenzioni sono vicine a quelle di [Leo Babauta (zenhabits)](https://zenhabits.net/uncopyright/), nonché di molti altri scrittori, artisti, sviluppatori e creativi che hanno fatto scelte simili.

Anche la prospettiva di [Derek Sivers](https://sive.rs/sharing) è interessante. A differenza sua, io per ora sono a quota **0** milioni di dollari. Forse avrei dovuto scegliere la licenza [CC0](https://creativecommons.org/publicdomain/zero/1.0/)!{{<emo "🤣️">}}

## Approfondimenti

* [Kirby Ferguson: <cite>Embrace the remix</cite> | TED Talk](https://www.ted.com/talks/kirby_ferguson_embrace_the_remix) (con sottotitoli e trascrizione in italiano disponibili)
* [FreedomDefined.org](https://freedomdefined.org/) (wiki sulle "Opere Culturali Libere", parzialmente tradotta in italiano)
* [About CC Licenses - Creative Commons](https://creativecommons.org/about/cclicenses/) (in inglese)

---

{{<q a="Hernando Fuentes" wa="Lana e Lilly Wachowski, J. Michael Straczynski" w="Sense8" d="S01E09" f="[" u="https://it.wikipedia.org/wiki/Sense8" >}}
Love, like art... must always be free.
{{</q>}}
