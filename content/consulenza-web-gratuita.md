---
title: "Consulenza gratuita"
description: "Per chiarire i tuoi dubbi, migliorare la tua comunicazione sul web e semplificare il tuo lavoro. In un'amichevole chiacchierata a tu per tu! 😊️"
---

Un'amichevole chiacchierata per migliorare la tua comunicazione sul web e semplificare la tua presenza online e il tuo lavoro.
{.big-text}

## Come funziona

Basta semplicemente [contattarmi](/contatti) e richiedere un appuntamento.

* Presentati in breve (e mandami il tuo sito, se ne hai uno).
* Puoi già proporre date e orari (altrimenti te ne proporrò io).
* Fammi sapere se hai richieste specifiche o bisogni particolari.

Concorderemo tutti i dettagli per email e fisseremo la nostra consulenza.
 
### Quanto dura

In genere **tra 30 e 60 minuti**, ma senza guardare troppo l'orologio.

Per me è meglio sforare di qualche minuto che lasciare discorsi in sospeso.

(La puntualità invece è una semplice questione di rispetto e buona educazione.)

### A che serve

* Risolvere i tuoi **dubbi** o i **problemi** legati al tuo sito.
* Rafforzare la tua **immagine professionale** online.
* Semplificare la **gestione** della tua presenza web.

### Fa per te se

* Punti sulla **qualità** e su una **comunicazione autentica** e rispettosa.
* Vuoi gestirti in **autonomia** per non essere alla mercé di nessuno.
* Sei ben consapevole del valore di **pace mentale** e **semplicità**.

### _Non_ fa per te se

* Dai più importanza all'**apparenza** che alla sostanza.
* Cerchi scorciatoie per non metterci **impegno** in prima persona.
* Insegui solo il **profitto immediato** a discapito di soluzioni sostenibili.

### Chi sono

Il _consulente web della **semplicità**_ che non sapevi di avere!{{<emo "😜️">}}

Lavoro all'incrocio tra **comunicazione** e **informatica**, con l'obiettivo dichiarato di aiutarti a smettere di complicarti inutilmente <del>la vita.</del><ins> Cioè, la presenza online.</ins>

Puoi scoprire qualcosina in più su di me nella pagina [Chi Sono](/chi-sono).

### Che faccio

* Metto a tua disposizione oltre 15 anni di **studi e ricerche**, per migliorare gli aspetti tecnici e strategici della tua comunicazione web.
* **Ti ascolto senza giudicarti**, per semplificare una situazione complessa e aiutarti a superare una fase di stallo o confusione.
* Quando meno te l'aspetti, tiro fuori una battuta stupida. Non so se ti farà ridere o piangere, ma almeno non ti annoierai!{{<emo "😂️">}}

### Che _non_ faccio

* Non spreco il tuo **tempo**: se penso ci siano soluzioni migliori per te rispetto a quelle che potrei darti io, te lo dico subito.
* Non ti carico di **pressione**: posso darti tutti i pareri spassionati che vuoi, ma lascio le scelte a te (senza subdole manipolazioni).
* Ultimo ma non meno importante: **non ti prendo in giro**. Ti parlo sempre _da persona a persona_, ancor prima che _da consulente a cliente_.{{<emo "🥰️">}}

### Dove ci vediamo

Dovunque! Ti invierò il link per accedere alla consulenza circa 10 minuti prima dell'ora di inizio. Non serve registrarsi e funziona da PC, cellulare e tablet.

{{<box info>}}
[Jitsi Meet](https://meet.jit.si) è una piattaforma di videoconferenza gratuita, intuitiva e sicura (open source e crittografata end-to-end, per garantire anche una buona privacy).

Per i più nerd: ecco il [repo GitHub di Jitsi Meet](https://github.com/jitsi/jitsi-meet) e una lista di [istanze pubbliche](https://jitsi.github.io/handbook/docs/community/community-instances/).
{{</box>}}

### Quando ci vediamo

Dipende da te, ormai sai già come funziona.{{<emo "😎️">}}

[Contattami](/contatti) e facciamoci quest'amichevole chiacchierata a tu per tu!{{<emo "😁️">}}

## Domande e risposte

{{<details "### Perché fai consulenze gratis?">}}
Trovo sia il modo migliore per **valutare se possiamo lavorare bene insieme**.

_Conviene a te_, così non dovrai comprare un servizio a scatola chiusa.

E _aiuta me_, che voglio attenuare il rischio di clienti insoddisfatti.
{{</details>}}

{{<details "### Ma non è che poi mi rifili i tuoi servizi?">}}
Faccio il consulente, non il venditore. Niente _fuffa a soli 997 €_ qui!{{<emo "😱️">}}

Il mio lavoro è farti capire **cos'è meglio _per te_**, anche se è peggio _per me_.

Inoltre, il marketing aggressivo e manipolatorio va contro tutti [i miei valori](/chi-sono/#valori).
{{</details>}}  

{{<details "### E se non avessi nulla in particolare da chiederti?">}}
Un parere tecnico sulla tua comunicazione web può sempre tornarti utile.

Dopotutto, per un occhio esterno è più facile notare problemi e soluzioni.

[Contattami](/contatti/) se vuoi fissare una sessione esplorativa in cerca di ispirazione!{{<emo "🤩️">}}
{{</details>}}

---

{{<q a="Henry David Thoreau" w="Walden: Vita nei boschi">}}
La nostra vita è sperperata in dettagli. \[...] Semplicità, semplicità, semplicità!
{{</q>}}
